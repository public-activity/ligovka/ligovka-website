import BudgetCodesList from '../components/Codes/BudgetCodesList'
import BudgetDetailsList from '../components/Details/BudgetDetailsList'
import BudgetDetailsPage from '../components/Details/BudgetDetailsPage'
import BudgetPresentationsList from '../components/Presentations/BudgetPresentationsList'
import BudgetPresentationPage from '../components/Presentations/BudgetPresentationPage'

export const budgetRoutes = [
    {
        path: '/budgets/codes',
        name: 'budget-codes-list',
        component: BudgetCodesList,
        props: { municipalityId: 1 }
    },
    {
        path: '/budgets',
        name: 'budget-details-list',
        component: BudgetDetailsList,
        props: { municipalityId: 1 }
    },
    {
        path: '/budgets/:detailsId(\\d+)/:presentationId(\\d+)?',
        name: 'budget-details',
        component: BudgetDetailsPage,
        props: (route) => ({
            detailsId: parseInt(route.params.detailsId),
            presentationId: parseInt(route.params.presentationId),
            codeTypeInput: route.query.codeType,
            viewTypeInput: route.query.viewType
        })
    },
    {
        path: '/budgets/presentations',
        name: 'budget-presentations-list',
        component: BudgetPresentationsList,
        props: { municipalityId: 1 }
    },
    {
        path: '/budgets/presentations/:presentationId',
        name: 'budget-presentation',
        component: BudgetPresentationPage,
        props: (route) => ({ presentationId: parseInt(route.params.presentationId) })
    },
]

function parseInt(value) {
    const intValue = Number.parseInt(value, 10);
    if (Number.isNaN(intValue)) {
        return null;
    }
    return intValue;
}

function parseBool(value) {
    if (value) {
        return value == 'true'
    }
    return null;
}