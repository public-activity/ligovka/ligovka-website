import { instance } from '@/services/http-common'

export class BudgetCodesService {
    get(municipalityId, codeType, includeInactive = false) {
        return instance.get('/BudgetCodes', { params: { municipalityId, codeType, includeInactive }});
    }
    getById(codeId) {
        return instance.get(`/BudgetCodes/${codeId}`);
    }
    create(data) {
        return instance.post('/BudgetCodes', data);
    }
    update(data) {
        return instance.put('/BudgetCodes', data);
    }
    delete(codeId) {
        return instance.delete(`/BudgetCodes/${codeId}`);
    }
}