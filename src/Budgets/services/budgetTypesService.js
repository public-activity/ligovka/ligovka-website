import { instance } from '@/services/http-common'

export class BudgetTypesService {
    get() {
        return instance.get('/BudgetTypes');
    }
}