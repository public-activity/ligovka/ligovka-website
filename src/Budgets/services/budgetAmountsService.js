import { instance } from '@/services/http-common'

export class BudgetAmountsService {
    create(detailsId, codeId, value) {
        return instance.post('/BudgetAmounts', { detailsId, codeId, value });
    }
    update(detailsId, codeId, value) {
        return instance.put('/BudgetAmounts', { detailsId, codeId, value });
    }
    delete(detailsId, codeId) {
        return instance.delete('/BudgetAmounts', { params: { detailsId, codeId }});
    }
}