import { codeTypes } from '@/services/values'

function getItemsForGraphDisplay(sourceItems, codeType) {
    let items = sourceItems
        .map(item => ({...item}))
        .filter(item => isItemForGraph(item));
    items.forEach(item => item.links = []);

    let incomes = items.filter(item => item.codeType.toLowerCase() == codeTypes.INCOME);
    let topIncomes = incomes.filter(item => item.parentsRelations.length == 0);
    let totalIncomeAmount = topIncomes.reduce((sum, {childrenAmount}) => sum + childrenAmount, 0);

    let expenditures = items.filter(item => item.codeType.toLowerCase() == codeTypes.EXPENDITURE);
    let topExpenditures = expenditures.filter(item => item.parentsRelations.length == 0);
    let totalExpenditureAmount = topExpenditures.reduce((sum, {childrenAmount}) => sum + childrenAmount, 0);

    if (codeType == codeTypes.INCOME) {
        processIncomes(incomes);
        return incomes;
    }
    else if (codeType == codeTypes.EXPENDITURE) {
        processExpenditures(expenditures);
        return expenditures;
    }
    else {
        processIncomes(incomes);
        processExpenditures(expenditures);

        let total = {
            aliasId: -1,
            aliasName: 'БЮДЖЕТ',
            isDisplayed: true,
            links: []
        };

        if (totalIncomeAmount < totalExpenditureAmount) {
            let surplusAmount = totalExpenditureAmount - totalIncomeAmount;
            let surplus = {
                aliasId: -2,
                aliasName: 'ДЕФИЦИТ',
                childrenAmount: surplusAmount,
                isDisplayed: true,
                links: [{
                    targetId: total.aliasId,
                    value: surplusAmount
                }]
            };
            items.push(surplus);

            total.childrenAmount = totalExpenditureAmount;
        }
        else if (totalIncomeAmount > totalExpenditureAmount){
            let shortfallAmount = totalIncomeAmount - totalExpenditureAmount;
            let shortfall = {
                aliasId: -3,
                aliasName: 'ПРОФИЦИТ',
                childrenAmount: shortfallAmount,
                isDisplayed: true,
                links: []
            };
            items.push(shortfall);

            total.links.push({
                targetId: shortfall.aliasId,
                value: shortfallAmount
            });
            total.childrenAmount = totalIncomeAmount;
        }
        else {
            total.childrenAmount = totalExpenditureAmount;
        }

        for (let i = 0; i < topIncomes.length; i++) {
            topIncomes[i].links.push({
                targetId: total.aliasId,
                value: topIncomes[i].childrenAmount
            });
        }

        for (let i = 0; i < topExpenditures.length; i++) {
            total.links.push({
                targetId: topExpenditures[i].aliasId,
                value: total.childrenAmount
            });
        }

        items.push(total);
        return items;
    }
}

function processIncomes(incomes) {
    for (let i = 0; i < incomes.length; i++) {
        let income = incomes[i];
        for(let j = 0; j < income.parentsRelations.length; j++) {
            let relation = income.parentsRelations[j];
            if (isItemForGraph(income) && relation.isPrimary) {
                income.links.push({
                    targetId: relation.parentAliasId,
                    value: income.childrenAmount
                });
            }
        }
    }
    return incomes;
}

function processExpenditures(expenditures) {
    for (let i = 0; i < expenditures.length; i++) {
        let expenditure = expenditures[i];
        for(let j = 0; j < expenditure.childrenRelations.length; j++) {
            let relation = expenditure.childrenRelations[j];
            let child = expenditures.find(item => item.aliasId == relation.childAliasId);
            if (isItemForGraph(child) && relation.isPrimary) {
                expenditure.links.push({
                    targetId: relation.childAliasId,
                    value: child.childrenAmount
                });
            }
        }
    }
    return expenditures;
}

function isItemForGraph(item) {
    return item && item.isDisplayed && item.childrenAmount > 0;
}

export function getPlotlyData(sourceItems, codeType) {
    let itemsForDisplay = getItemsForGraphDisplay(sourceItems, codeType);
    console.log(itemsForDisplay);
    let data = {
        nodes: [],
        links: []
    };

    for (let i = 0; i < itemsForDisplay.length; i++) {
        let item = itemsForDisplay[i];
        let node = {
            aliasId: item.aliasId,
            aliasName: item.aliasName,
            childrenAmount: item.childrenAmount,
            color: Math.floor(Math.random()*16777215).toString(16)
        }
        data.nodes.push(node);

        for(let j = 0; j < item.links.length; j++) {
            let link = item.links[j];
            let targetIndex = itemsForDisplay.findIndex(element => element.aliasId == link.targetId);
            console.log(targetIndex);
            if (targetIndex >= 0) {
                data.links.push({
                    source: i,
                    target: targetIndex,
                    value: link.value
                })
            }
        }
    }

    return data;
}