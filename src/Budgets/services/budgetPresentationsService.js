import { instance } from '@/services/http-common'

export class BudgetPresentationsService {
    get(municipalityId) {
        return instance.get('/BudgetPresentations', { params: { municipalityId }});
    }
    getById(presentationId) {
        return instance.get(`/BudgetPresentations/${presentationId}`);
    }
    create(data) {
        return instance.post('/BudgetPresentations', data);
    }
    update(data) {
        return instance.put('/BudgetPresentations', data);
    }
    delete(presentationId) {
        return instance.delete(`/BudgetPresentations/${presentationId}`);
    }
}