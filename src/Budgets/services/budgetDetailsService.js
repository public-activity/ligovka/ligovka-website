import { instance } from '@/services/http-common'

export class BudgetDetailsService {
    get(municipalityId) {
        return instance.get('/BudgetDetails', { params: { municipalityId }});
    }
    getById(detailsId) {
        return instance.get(`/BudgetDetails/${detailsId}`);
    }
    create(data) {
        return instance.post('/BudgetDetails', data);
    }
    update(data) {
        return instance.put('/BudgetDetails', data);
    }
    delete(detailsId) {
        return instance.delete(`/BudgetDetails/${detailsId}`);
    }
}