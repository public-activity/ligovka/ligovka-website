import { instance } from '@/services/http-common'

export class BudgetAliasesService {
    get(presentationId, codeType) {
        return instance.get('/BudgetAliases', { params: { presentationId, codeType }});
    }
    getById(aliasId) {
        return instance.get(`/BudgetAliases/${aliasId}`);
    }
    create(data) {
        return instance.post('/BudgetAliases', data);
    }
    update(data) {
        return instance.put('/BudgetAliases', data);
    }
    delete(aliasId) {
        return instance.delete(`/BudgetAliases/${aliasId}`);
    }
}