import { instance } from '@/services/http-common'

export class BudgetViewsService {
    get(detailsId, presentationId, codeType) {
        let params = {};
        if (codeType != undefined)
            params.codeType = codeType;
        return instance.get(`/BudgetViews/${detailsId}/${presentationId}`, { params });
    }
}