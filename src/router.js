import { createWebHistory, createRouter } from 'vue-router';
import { budgetRoutes } from '@/Budgets/services/budgetRoutes'
import Deputy from '@/components/Deputy/Deputy'
import Documents from '@/components/Documents/Documents'
import Map from '@/components/Map/Map'
import Purchases from '@/components/Purchases/Purchases'
import News from '@/components/News/News'
import PageNotFound from '@/components/PageNotFound'

const routes = [
    ...budgetRoutes,
    {
        path: '/',
        alias: '/news',
        name: 'news',
        component: News
    },
    {
        path: '/deputy',
        name: 'deputy',
        component: Deputy
    },
    {
        path: '/documents',
        name: 'documents',
        component: Documents
    },
    {
        path: '/map',
        name: 'map',
        component: Map
    },
    {
        path: '/purchases',
        name: 'purchases',
        component: Purchases
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: PageNotFound
    }
]

export const router = createRouter({
    history: createWebHistory(),
    routes: routes,
});