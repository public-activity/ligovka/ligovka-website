import axios from 'axios';

export const instance = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
    headers: {
        'Content-type': 'application/json'
    }
});

export const defaultHandleErrorFunction = function (error) {
    if (error.response) {
        // Request made and server responded
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
    } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
    }
};
