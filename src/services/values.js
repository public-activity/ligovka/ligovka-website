export const budgetTypes = {
    PROJECT: 'project',
    DECISION: 'decision',
    UTILIZATION: 'utilization'
};

export const codeTypes = {
    INCOME: 'income',
    EXPENDITURE: 'expenditure'
};

export const viewTypes = {
    TABLE: {
        value: 'table',
        name: 'Таблица'
    },
    TREE: {
        value: 'tree',
        name: 'Дерево'
    },
    GRAPH: {
        value: 'graph',
        name: 'График'
    }
};