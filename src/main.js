import { createApp } from 'vue'
import { router } from '@/router'
import VCalendar from 'v-calendar';
import App from '@/App.vue'

const app = createApp(App);
app.use(router);
app.use(VCalendar);
app.mount('#app');