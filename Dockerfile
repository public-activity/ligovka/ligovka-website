FROM node:lts-alpine as build_environment
LABEL product="LigovkaBudget WebApi" stage="build_environment"
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:stable-alpine as runtime_environment
LABEL product="Ligovka WebSite" stage="runtime_environment"
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
COPY --from=build_environment /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]